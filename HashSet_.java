import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class HashSet_ {
    public static void main(String[] args) {
        // Constructs a new, empty set; the backing HashMap instance has default initial
        // capacity (16) and load factor (0.75).
        HashSet<Integer> hs = new HashSet<>();
        System.out.println(hs);

        // Constructs a new set containing the elements in the specified collection.
        List<Integer> ls = new ArrayList<>();
        ls.add(52);
        ls.add(86);
        ls.add(76);
        ls.add(21);
        HashSet<Integer> hz = new HashSet<>(ls);
        System.out.println(hz);

        HashSet<String> h_set = new HashSet<String>();

        // Use add() method to add elements into the Set
        h_set.add("Welcome");
        h_set.add("To");
        h_set.add("Geeks");
        h_set.add("4");
        h_set.add("oiuyt");
        h_set.add("6");
        h_set.add("Geeks");
        System.out.println("HashSet: " + h_set);

        // contains(Object o)
        boolean contains = h_set.contains("4");
        System.out.println(contains);

        // Removing elements
        // using remove() method
        h_set.remove("Welcome");
        System.out.println(h_set);

        /**
         * Difference - Operation
         * Remove All Elements From Another Collection
         * The Java Set interface as a method called removeAll() which removes all
         * elements in the Set which are also present in another Collection. In set
         * theory, this is referred to as the difference between the Set and the other
         * Collection.
         */
        HashSet<String> d_set = new HashSet<String>();
        d_set.add("Geeks");
        d_set.add("4");
        h_set.removeAll(d_set);
        System.out.println(h_set);

        /**
         * Intersection - Operation
         * Retain All Elements Present in Another Collection
         * The Java Set interface also has a method which retains all elements in the
         * Set which are also present in another Collection. All elements found in the
         * Set which are not present in the other Collection will be removed. In set
         * theory this is referred to as the intersection between the Set and the other
         * Collection.
         */
        HashSet<String> set1 = new HashSet<>();
        set1.add("one");
        set1.add("two");
        set1.add("three");
        set1.add("four");
        set1.add("five");

        HashSet<String> set2 = new HashSet<>();
        set2.add("three");
        set2.add("four");
        set2.add("fifteen");
        set2.add("forteen");

        set1.retainAll(set2);

        System.out.println(set1);

        // isEmpty()
        System.out.println("h_set is empty ? : " + h_set.isEmpty());
        System.out.println("hs is empty ? : " + hs.isEmpty());

        // size()
        System.out.println("h_set is size = " + h_set.size());
        System.out.println("hs is size = " + hs.size());

        // The toArray() method of Java Set is used to form an array of the same
        // elements as that of the Set.
        Object[] arr = h_set.toArray();

        /**
         * Convert Java Set to List
         * You can convert a Java Set to a Java List by creating a List and calling its
         * addAll() method, passing the Set as parameter to the addAll() method.
         */

        List<String> myls = new ArrayList<>();
        myls.addAll(h_set);

        /**
         * Iterate Set Elements
         * There are two ways to iterate the elements of a Java Set:
         * 
         * Using an Iterator obtained from the Set.
         * Using the for-each loop.
         * Both of these options are covered in the following sections.
         */

        Iterator<String> it = h_set.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " -> ");
        }
        System.out.println();

        for (Object idx : h_set) {
            idx.hashCode();
        }
    }
}

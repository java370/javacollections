import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class Queue_ {

    /**
     * add() - Inserts the specified element into the queue. If the task is
     * successful, add() returns true, if not it throws an exception.
     * offer() - Inserts the specified element into the queue. If the task is
     * successful, offer() returns true, if not it returns false.
     * element() - Returns the head of the queue. Throws an exception if the queue
     * is empty.
     * peek() - Returns the head of the queue. Returns null if the queue is empty.
     * remove() - Returns and removes the head of the queue. Throws an exception if
     * the queue is empty.
     * poll() - Returns and removes the head of the queue. Returns null if the queue
     * is empty.
     * 
     * @param args
     */
    public static void main(String[] args) {
        // Creating Queue using the LinkedList class
        Queue<Integer> numbers = new LinkedList<>();

        // offer elements to the Queue
        numbers.offer(1);
        numbers.offer(2);
        numbers.offer(3);
        System.out.println("Queue: " + numbers);

        // Access elements of the Queue
        int accessedNumber = numbers.peek();
        System.out.println("Accessed Element: " + accessedNumber);

        // Remove elements from the Queue
        int removedNumber = numbers.poll();
        System.out.println("Removed Element: " + removedNumber);

        System.out.println("Updated Queue: " + numbers);

        // Check if Queue Contains Element
        boolean v = numbers.contains(3);
        System.out.println(v);

        // Iterate All Elements in Queue
        Iterator<Integer> it = numbers.iterator();
        while (it.hasNext()) {
            Integer element = it.next();
        }

        // access via new for-loop
        for (Integer element : numbers) {
            // do something with each element
        }

    }
}

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;

public class List_ {
    public static void main(String[] args) {
        List<Integer> m = new ArrayList<>();
        // simple add
        m.add(10);
        m.add(20);
        m.add(30);
        m.add(40);

        System.out.println(m);

        // Add specific Index
        // add(Index,Value)
        // ** add method can also update any Index value
        m.add(2, 4);
        System.out.println(m);

        List<Integer> n = new ArrayList<>();
        n.add(6);
        n.add(3);
        n.add(7);

        // Add new arraylist to a list to the last of the list
        m.addAll(n);
        System.out.println(m);

        List<Integer> k = new ArrayList<>();
        k.add(88);
        k.add(20);
        k.add(33);
        // Add an arraylist to specific position of another list
        m.addAll(2, k);
        System.out.println(m);

        // clear a list
        n.clear();
        System.out.println(n);

        // Iterate List using Iterator
        // Iterator<E> iterator()

        Iterator<Integer> it = m.iterator();
        while (it.hasNext()) {
            System.out.print(it.next() + " - ");
        }
        System.out.println();

        // Access elements of list foreach loop
        for (Integer idx : m) {
            System.out.print(idx + " ");
        }
        System.out.println();

        // Access elements of list for loop
        for (int i = 0; i < m.size(); i++) {
            System.out.print(m.get(i) + " ");
        }
        System.out.println();

        // contains(Object o)
        System.out.println(m.contains(30));
        System.out.println(m.contains(300));

        // containsAll(Collection<?> c)
        List<Integer> p = new ArrayList<>();
        p.add(88);
        p.add(20);
        p.add(33);
        List<Integer> q = new ArrayList<>();
        q.add(88);
        q.add(20);
        q.add(33);
        // if list contains all the values of another list
        System.out.println(p.containsAll(q));
        ;

        // equals(Object o) // list comparison
        System.out.println(p.equals(q));

        // hashCode()
        System.out.println(p.hashCode() + " " + q.hashCode());

        // indexof = first occurance of the element
        // indexof(value)
        int fpos = m.indexOf(20);
        System.out.println(fpos);

        // lastindexof = first occurance of the element from the last
        // lastindexof(value)
        int lpos = m.lastIndexOf(20);
        System.out.println(lpos);

        // remove(int index)
        m.remove(2);
        System.out.println(m);

        // remove(Object o) Convert primitive type to wrapper class object
        m.remove(Integer.valueOf(4));
        System.out.println(m);

        m.addAll(p);
        // removeAll(Collection<?> c)
        // removes all the contents of this list containg list
        m.removeAll(p);
        System.out.println(m);

        // boolean retainAll(Collection<?> c)
        // Retains only the common elements of both lists

        List<String> list = new ArrayList<>();
        List<String> otherList = new ArrayList<>();

        String element1 = "element 1";
        String element2 = "element 2";
        String element3 = "element 3";
        String element4 = "element 4";
        String element5 = "element 5";

        list.add(element1);
        list.add(element2);
        list.add(element3);
        list.add(element5);

        otherList.add(element1);
        otherList.add(element3);
        otherList.add(element4);
        otherList.add(element5);

        list.retainAll(otherList);

        System.out.println(list);

        // Update any Index value
        // set(int index, E element)
        m.set(0, 9);
        System.out.println(m);

        // size()
        System.out.println(m.size());

        // subList(int fromIndex, int toIndex)
        // Returns a view of the portion of this list between
        // the specified fromIndex, inclusive, and toIndex, exclusive.
        System.out.println(m.subList(2, 5));

        // isEmpty() true | false
        System.out.println(m.isEmpty());

        // public Object[] toArray()
        Object[] values = m.toArray();
        for (Object object : values) {
            System.out.print(object + " ");
        }
        System.out.println();

    }
}

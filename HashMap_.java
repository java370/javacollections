import java.security.Key;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.function.BiConsumer;

public class HashMap_ {
    public static void main(String[] args) {
        // Initialization of a HashMap using Generics
        HashMap<Integer, String> hm = new HashMap<Integer, String>();

        // Add elements to a HashMap
        // Adding elements using put method
        // Custom input elements
        hm.put(1, "one");
        hm.put(2, "two");
        hm.put(3, "three");
        hm.put(4, "four");
        hm.put(5, "five");
        hm.put(6, "six");

        // Java HashMap putAll()

        /**
         * The Java HashMap putAll() method inserts all the key/value mappings from the
         * specified Map to the HashMap.
         */
        HashMap<Integer, String> yy = new HashMap<Integer, String>();
        yy.put(567, "fss");
        yy.put(789, "sen");
        yy.put(324, "ttf");
        yy.put(664, "ssf");

        hm.putAll(yy);

        /**
         * putIfAbsent()
         * The Java HashMap putIfAbsent() method inserts the specified key/value mapping
         * to the hashmap if the specified key is already not present in the hashmap.
         */

        // key already not present in HashMap
        hm.putIfAbsent(7, "seven");

        // key already present in HashMap
        hm.putIfAbsent(2, "tow-tow");

        /**
         * computeIfAbsent()
         * The Java HashMap computeIfAbsent() method computes a new value and associates
         * it with the specified key if the key is not associated with any value in the
         * hashmap.
         * hashmap.computeIfAbsent(K key, Function remappingFunction)
         */
        hm.computeIfAbsent(101, key -> "hundred one");
        /**
         * computeIfPresent()
         * The Java HashMap computeIfPresent() method computes a new value and
         * associates it with the specified key if the key is already present in the
         * hashmap.
         */

        hm.computeIfPresent(7, (key, value) -> value.toUpperCase());

        // containsKey()
        if (hm.containsKey(1)) {
            System.out.println("Present");
        } else
            System.out.println("Abset");
        // containsValue()
        if (hm.containsValue("seviio")) {
            System.out.println("Present");
        } else
            System.out.println("Abset");

        // Replacing an Entry in a Java Map

        /**
         * The replace() method can take 3 parameters.
         * 
         * key - key whose mapping is to be replaced
         * oldValue (optional)- value to be replaced in the mapping
         * newValue - oldValue is replaced with this value
         * 
         */

        // replace(K key, V value)
        hm.replace(6, "seven");

        /**
         * replaceAll()
         * The Java HashMap replaceAll() method replaces all mappings of the hashmap
         * with the result from the specified function.
         */

        hm.replaceAll((key, value) -> value.toUpperCase());

        // get(Object key)

        String value_of_key = hm.get(2);
        System.out.println(value_of_key);

        // getOrDefault()
        // mapping for the key is present in HashMap
        String value1 = hm.getOrDefault(1, "Not Found");
        System.out.println("Value for key 1:  " + value1);
        // mapping for the key is not present in HashMap
        String value2 = hm.getOrDefault(101, "Not Found");
        System.out.println("Value for key 101: " + value2);

        // HashMap(Map map) Constructor initialization
        HashMap<Integer, String> hm2 = new HashMap<Integer, String>(hm);
        System.out.println(hm2);

        // Iterating the Entries of a Java Map

        // iterate through keys only
        System.out.print("Keys: ");
        for (Integer key : hm.keySet()) {
            System.out.print(key);
            System.out.print(", ");
        }

        // iterate through values only
        System.out.print("\nValues: ");
        for (String value : hm.values()) {
            System.out.print(value);
            System.out.print(", ");
        }

        /**
         * It is also possible to iterate all entries of a Java Map. By entries I mean
         * key + value pairs. An entry contains both the key and value for that entry.
         * Earlier we have only iterated either the keys, or the values, but by
         * iterating the entries we iterate both at the same time.
         * 
         * Like with keys and values, there are two ways to iterate the entries of a
         * Map:
         * 
         * Using the for-each loop
         */

        // Iterator<Map.Entry <Integer, String>> it = hm. ;
        for (Entry<Integer, String> it : hm.entrySet()) {
            System.out.println(it.getKey() + " -- " + it.getValue());
        }

        // Iterate using Lambda Function
        // inner ForEach Loop

        hm.forEach((k, v) -> System.out.println(k + " = " + v));

        // using BiConsumer Functional Interface
        BiConsumer<Integer, String> biConsumer = (x, y) -> System.out.println(x + " -*- " + y);
        hm.forEach(biConsumer);

        hm.forEach(new BiConsumer<Integer, String>() {
            public void accept(Integer t, String u) {
                System.out.println(t + " -- " + u);
            };
        });

    }
}

/**
 * Performance of HashMap depends on 2 parameters which are named as follows:
 * 
 * Initial Capacity
 * Load Factor
 * 1. Initial Capacity – It is the capacity of HashMap at the time of its
 * creation (It is the number of buckets a HashMap can hold when the HashMap is
 * instantiated). In java, it is 2^4=16 initially, meaning it can hold 16
 * key-value pairs.
 * 
 * 2. Load Factor – It is the percent value of the capacity after which the
 * capacity of Hashmap is to be increased (It is the percentage fill of buckets
 * after which Rehashing takes place). In java, it is 0.75f by default, meaning
 * the rehashing takes place after filling 75% of the capacity.
 * 
 * 3. Threshold – It is the product of Load Factor and Initial Capacity. In
 * java, by default, it is (16 * 0.75 = 12). That is, Rehashing takes place
 * after inserting 12 key-value pairs into the HashMap.
 * 
 * 4. Rehashing – It is the process of doubling the capacity of the HashMap
 * after it reaches its Threshold. In java, HashMap continues to rehash(by
 * default) in the following sequence – 2^4, 2^5, 2^6, 2^7, …. so on.
 * 
 * 
 * If the initial capacity is kept higher then rehashing will never be done. But
 * by keeping it higher increases the time complexity of iteration. So it should
 * be chosen very cleverly to increase performance. The expected number of
 * values should be taken into account to set the initial capacity. The most
 * generally preferred load factor value is 0.75 which provides a good deal
 * between time and space costs. The load factor’s value varies between 0 and 1.
 */

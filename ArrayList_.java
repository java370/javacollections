import java.util.ArrayList;

public class ArrayList_ {
    public static void main(String[] args) {
        ArrayList<Integer> m = new ArrayList<>();

        // simple add
        m.add(10);
        m.add(20);
        m.add(30);
        m.add(40);

        System.out.println(m);

        // Add specific Index
        // add(Index,Value)
        m.add(2, 4);
        System.out.println(m);

        ArrayList<Integer> n = new ArrayList<>();
        n.add(6);
        n.add(3);
        n.add(7);

        // Add new arraylist to a list to the last of the list
        m.addAll(n);
        System.out.println(m);

        ArrayList<Integer> k = new ArrayList<>();
        k.add(88);
        k.add(20);
        k.add(33);
        // Add an arraylist to specific position of another list
        m.addAll(2, k);
        System.out.println(m);

        // clear a list
        n.clear();
        System.out.println(n);

        // Access elements of arraylist
        for (Integer idx : m) {
            System.out.print(idx + " ");
        }
        for (int i = 0; i < m.size(); i++) {
            System.out.print(m.get(i) + " ");
        }
        System.out.println();

        // indexof = first occurance of the element
        // indexof(value)
        int fpos = m.indexOf(20);
        System.out.println(fpos);

        // lastindexof = first occurance of the element from the last
        // lastindexof(value)
        int lpos = m.lastIndexOf(20);
        System.out.println(lpos);

        // remove(int index)
        m.remove(2);
        System.out.println(m);

        // remove(Object o) Convert primitive type to wrapper class object
        m.remove(Integer.valueOf(4));
        System.out.println(m);

        // removeRange(int fromIndex, int toIndex) [inclusive,exclusive)
        // m.removeRange(0,2);

        // set(int index, E element)
        m.set(0, 9);
        System.out.println(m);

        // size()
        System.out.println(m.size());

        // subList(int fromIndex, int toIndex)
        // Returns a view of the portion of this list between
        // the specified fromIndex, inclusive, and toIndex, exclusive.
        System.out.println(m.subList(2, 7));

        // public Object[] toArray()
        Object[] values = m.toArray();
        for (Object object : values) {
            System.out.print(object + " ");
        }
        System.out.println();

        // isEmpty() true | false
        System.out.println(m.isEmpty());

    }
}
